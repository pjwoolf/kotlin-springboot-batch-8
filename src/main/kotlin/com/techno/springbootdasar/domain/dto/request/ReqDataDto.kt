package com.techno.springbootdasar.domain.dto.request

import javax.validation.constraints.*

data class ReqDataDto(
    @field:NotBlank(message = "notNullRequest is not blank")
    val notNullRequest: String?,
    @field:NotEmpty(message = "notEmptyRequest")
    val notEmptyRequest: String?,
    @field:NotNull(message = "minMaxRequest is not null")
    @field:NotEmpty(message = "minMaxRequest is not empty")
    @field:Size(min = 5, max = 10)
    val minMaxRequest: String?,
    @field:Positive(message = "positiveRequest only positive")
    val positiveRequest: Int?,
    @field:PositiveOrZero(message = "positiveOrZeroRequest")
    val positiveOrZeroRequest: Int?,
    @field:Negative(message = "negativeRequest")
    val negativeRequest: Int?,
    @field:NegativeOrZero(message = "negativeOrZeroRequest")
    val negativeOrZeroRequest: Int?,
    @field:NotNull(message = "patternRequest is not null")
    @field:NotEmpty(message = "patternRequest is not empty")
    @field:Pattern(regexp = "^[a-zA-z]*$")
    val patternRequest: String?,

    )
