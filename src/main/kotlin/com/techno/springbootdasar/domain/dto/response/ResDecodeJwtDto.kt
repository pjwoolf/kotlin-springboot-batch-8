package com.techno.springbootdasar.domain.dto.response

data class ResDecodeJwtDto(
    val id : String = "",
    val email : String
)
