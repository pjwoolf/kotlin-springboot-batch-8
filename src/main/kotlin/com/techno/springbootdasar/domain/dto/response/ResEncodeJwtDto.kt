package com.techno.springbootdasar.domain.dto.response

data class ResEncodeJwtDto(
    val id: String = "",
    val token: String = "",
)
