package com.techno.springbootdasar.domain.dto.response

import com.fasterxml.jackson.annotation.JsonProperty

data class ResLoginDto(
    @field:JsonProperty(value = "id_user")
    val idUser : String? = null,
    val email: String = "",
    val token: String = "",
)
