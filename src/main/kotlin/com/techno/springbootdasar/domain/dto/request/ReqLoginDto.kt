package com.techno.springbootdasar.domain.dto.request

data class ReqLoginDto(
    val email: String = "",
    val password: String = ""
)
