package com.techno.springbootdasar.controller

import com.techno.springbootdasar.domain.dto.request.ReqUserDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResUserDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/api")
class TestController {
    val firstName = "Park"
    val lastName = "Jeongwoo"

    @GetMapping("/test")
    fun testGetMapping(): ResponseEntity<LinkedHashMap<String, String>> {
        val response : LinkedHashMap<String, String> = LinkedHashMap()
        response["first_name"] = firstName
        response["last_name"] = lastName

        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/user")
    fun getName(@RequestParam("age") age : String): ResponseEntity<LinkedHashMap<String, String>> {
        val response : LinkedHashMap<String, String> = LinkedHashMap()
        response["first_name"] = firstName
        response["last_name"] = lastName
        response["age"] = age

        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/user/{age}")
    fun getNameByPath(@PathVariable("age") age : String): ResponseEntity<LinkedHashMap<String, String>> {
        val response : LinkedHashMap<String, String> = LinkedHashMap()
        response["first_name"] = firstName
        response["last_name"] = lastName
        response["age"] = age

        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/user/dto")
    fun getUser(@RequestParam("age") age : Int): ResponseEntity<ResUserDto> {
        val response = ResUserDto(
            firstName = this.firstName,
            lastName = this.lastName,
            age = age
        )

        return ResponseEntity.ok().body(response)
    }

    @PostMapping("/user")
    fun postUser(@RequestBody reqUserDto: ReqUserDto): ResponseEntity<ResBaseDto<ResUserDto>> {
        val data = ResUserDto(
            firstName = reqUserDto.firstName,
            lastName = reqUserDto.lastName,
            age = reqUserDto.age
        )

        val response = ResBaseDto(data = data)

        return ResponseEntity.ok().body(response)
    }
}