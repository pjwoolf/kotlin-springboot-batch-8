package com.techno.springbootdasar.service

interface LogicService {
    fun oddOrEven(number : Int) : String
}