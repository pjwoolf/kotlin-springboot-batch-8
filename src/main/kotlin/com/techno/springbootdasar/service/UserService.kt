package com.techno.springbootdasar.service

import com.techno.springbootdasar.domain.dto.request.ReqLoginDto
import com.techno.springbootdasar.domain.dto.request.ReqUserDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResLoginDto
import com.techno.springbootdasar.domain.dto.response.ResUserDto

interface UserService {
    fun getUserAll() : ResBaseDto<ArrayList<ResUserDto>>
    fun getUserById(idUser: String) : ResBaseDto<ResUserDto>
    fun insertUser(reqUserDto: ReqUserDto) : ResBaseDto<ResUserDto>
    fun updateUser(reqUserDto: ReqUserDto, idUser: String) : ResBaseDto<ResUserDto>
    fun deleteUser(idUser: String) : ResBaseDto<ResUserDto>

    fun loginUser(reqLoginDto: ReqLoginDto) : ResBaseDto<ResLoginDto>
}