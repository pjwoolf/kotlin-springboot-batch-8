package com.techno.springbootdasar.util

import io.jsonwebtoken.Claims
import io.jsonwebtoken.JwtBuilder
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import java.util.*
import javax.crypto.spec.SecretKeySpec
import javax.xml.bind.DatatypeConverter


class JWTGenerator {
    companion object {
        private const val SECRET_KEY = "SUPER_SECRETE"
        private val instance: JWTGenerator = JWTGenerator()
    }

    fun createJWT(id : String, subject: String): String {
        val signatureAlgorithm: SignatureAlgorithm = SignatureAlgorithm.HS256
        val nowMills: Long = System.currentTimeMillis()
        val now = Date(nowMills)

        val apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY)
        val signingKey = SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.jcaName)

        val builder: JwtBuilder = Jwts.builder().setId(id)
            .setIssuedAt(now)
            .setSubject(subject)
            .setIssuer("technocenter")
            .setAudience("technocenter")
            .signWith(signatureAlgorithm, signingKey)

        val expMills = nowMills + 86400000L
        val exp = Date(expMills)
        builder.setExpiration(exp)

        return builder.compact()
    }

    fun decodeJWT(jwt: String): Claims {
        val claims: Claims = Jwts.parser()
            .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
            .parseClaimsJws(jwt).body

        println("ID : ${claims.id}")
        println("Issuer : ${claims.issuer}")
        println("Subject : ${claims.subject}")
        return claims
    }
}