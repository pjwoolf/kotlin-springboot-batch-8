package com.techno.springbootdasar.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.techno.springbootdasar.domain.common.CommonVariable
import com.techno.springbootdasar.domain.common.StatusCode
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class RequestInterceptor (
    @Value("\${header.request.api-key}")
    private val apiKey : String
) : HandlerInterceptor {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val apiKeyRequest = request.getHeader("APIKey")
        if (apiKeyRequest != apiKey) {
            val body : ResBaseDto<String> = ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = "API key failed.",
                data = null
            )
            internalServerError(body, response)
            return false
        }
        return super.preHandle(request, response, handler)
    }

    fun internalServerError(body: ResBaseDto<String>, response: HttpServletResponse): HttpServletResponse {
        response.status = HttpStatus.FORBIDDEN.value()
        response.contentType = "application/json"
        response.writer.write(convertObjectToJson(body))

        return response
    }

    fun convertObjectToJson(dto: ResBaseDto<String>): String? {
        return ObjectMapper().writeValueAsString(dto)
    }
}