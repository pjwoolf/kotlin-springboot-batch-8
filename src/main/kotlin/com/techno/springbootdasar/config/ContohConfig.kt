package com.techno.springbootdasar.config

import com.techno.springbootdasar.service.LogicService
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
class ContohConfig(
    private val logicService: LogicService
) {
    @Bean
    fun printName() {
        println("Hello my name is Jeongwoo")
    }

    @Bean
    fun printOddOrEven() {
        println("Nilai 5 = ${logicService.oddOrEven(5)}")
    }
}