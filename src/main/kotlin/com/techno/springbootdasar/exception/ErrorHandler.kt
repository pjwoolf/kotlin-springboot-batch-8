package com.techno.springbootdasar.exception

import com.techno.springbootdasar.domain.common.CommonVariable
import com.techno.springbootdasar.domain.common.StatusCode
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
class ErrorHandler {
    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleArgumentNotValidException(exception: MethodArgumentNotValidException): ResponseEntity<Any>{
        val errors = mutableListOf<String>()
        exception.bindingResult.fieldErrors.forEach{
            errors.add(it.defaultMessage!!)
        }
        val result = ResBaseDto(
            outStat = StatusCode.FAILED.code,
            outMess = errors.toString(),
            data = null
        )
        return ResponseEntity.badRequest().body(result)
    }

    @ExceptionHandler(RuntimeException::class)
    fun handleCustomException(exception: RuntimeException): ResponseEntity<Any>{
        exception.printStackTrace()
        val result = ResBaseDto(
            outStat = StatusCode.FAILED.code,
            outMess = CommonVariable.FAILED_MESSAGE,
            data = exception.message
        )
        return ResponseEntity.badRequest().body(result)
    }

}