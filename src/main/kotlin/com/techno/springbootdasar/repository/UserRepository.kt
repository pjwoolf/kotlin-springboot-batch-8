package com.techno.springbootdasar.repository

import com.techno.springbootdasar.domain.entity.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import java.util.*
import javax.transaction.Transactional
import kotlin.collections.Collection

interface UserRepository : JpaRepository<UserEntity, String> {
//    Query by method
    fun findByIdUser(idUser: UUID) : UserEntity?
    fun findByEmailAndPassword(email: String, password: String) : UserEntity?

//    Query raw
    @Query("SELECT a FROM UserEntity a WHERE idUser = :idUser")
    fun getByIdUser(idUser: UUID) : UserEntity?

//    Query raw native
    @Query("SELECT * FROM mst_user mu WHERE mu.uuid = :idUser", nativeQuery = true)
    fun getByIdUserNative(idUser: UUID) : Collection<List<String>>?

    @Modifying
    @Transactional
    fun deleteByIdUser(idUser: UUID) : Int?
}